package com.cimb.tcj.msonloading.authentication.controller;

import java.net.URI;
import java.util.Collections;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cimb.tcj.msonloading.authentication.beans.ApiResponse;
import com.cimb.tcj.msonloading.authentication.beans.JwtAuthenticationResponse;
import com.cimb.tcj.msonloading.authentication.beans.LoginRequest;
import com.cimb.tcj.msonloading.authentication.beans.SignUpRequest;
import com.cimb.tcj.msonloading.authentication.exception.AppException;
import com.cimb.tcj.msonloading.authentication.model.Role;
import com.cimb.tcj.msonloading.authentication.model.RoleName;
import com.cimb.tcj.msonloading.authentication.model.User;
import com.cimb.tcj.msonloading.authentication.repo.RoleRepo;
import com.cimb.tcj.msonloading.authentication.repo.UserRepo;
import com.cimb.tcj.msonloading.authentication.security.JwtTokenProvider;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepo userRepository;

	@Autowired
	RoleRepo roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@PostMapping("/generate-token")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		
		System.out.println("COMIN HERE ....");
		
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.ACCEPTED);
		} 
		

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				signUpRequest.getPassword(), signUpRequest.getPhoneno());

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		System.out.println("\n password: <<" + user.getPassword() + " >>");

		Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new AppException("User Role not set."));
		
		
		System.out.println("collection: \t " + Collections.singleton(userRole));
		
		user.setRoles(Collections.singleton(userRole));

		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUsername()).toUri();

//		return ResponseEntity.created(location).body(new ApiResponse(true, "USER SUCCESSFULLY CREATED!",location));
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}
	
	private String generateOTP() {
		return null;
		
	}
}
