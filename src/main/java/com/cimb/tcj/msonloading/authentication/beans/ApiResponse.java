package com.cimb.tcj.msonloading.authentication.beans;

import java.net.URI;

public class ApiResponse {

	private Boolean success;
	private String message;
	private URI uri;
	

	public ApiResponse(Boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}

	public ApiResponse(Boolean success, String message, URI uri ) {
		this.success = success;
		this.message = message;
		this.uri = uri;
	}

	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
