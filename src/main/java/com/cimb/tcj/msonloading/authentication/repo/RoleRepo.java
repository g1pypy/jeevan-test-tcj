package com.cimb.tcj.msonloading.authentication.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cimb.tcj.msonloading.authentication.model.Role;
import com.cimb.tcj.msonloading.authentication.model.RoleName;

@Repository
public interface RoleRepo extends JpaRepository<Role, Long> {

	Optional<Role> findByName(RoleName roleName);
}
