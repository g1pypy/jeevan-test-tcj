package com.cimb.tcj.msonloading.authentication.model;

public enum RoleName {

	ROLE_USER, ROLE_ADMIN, ROLE_CLERK
}
