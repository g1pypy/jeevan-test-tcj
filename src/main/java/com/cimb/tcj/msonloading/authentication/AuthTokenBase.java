package com.cimb.tcj.msonloading.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthTokenBase {

	public static void main(String[] args) {
		SpringApplication.run(AuthTokenBase.class, args);
	}
}
